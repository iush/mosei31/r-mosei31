---
title: "DPDiscreta"
author: "Oscar Munoz"
date: "10/22/2018"
output: 
  html_document: 
    keep_md: yes
---



## Modelos de Distribucion de Probabilidad Discreta

### Problema 1. Contestando al azar:

Un alumno contesta al azar un examen que contiene 15 preguntas, donde cada pregunta tiene 5 opciones de respuesta y solo una es la correcta. Si el alumno contesta todas las preguntas al azar.

a.  Calcular la probabilidad de que tenga menos de 6 preguntas correctas.
b.  Calcular la probabilidad de que tenga por lo menos 3 y a lo mucho 8 preguntas correctas.
c.  Calcular la probabilidad que aprueba el examen si la calificación de pase es 60.
d.  Obtener el número promedio de preguntas correctas en el examen y su desviación estándar.

Solucion:

Se sabe que solo 1 de 5 es la posible respuesta correcta, luego la probabilidad de responder a una pregunta correctamente es 1/5=0.2.

a. Podemos encontrar la probabilidad de tener 6 preguntas correctas respondiendo de manera aleatoria usando:

Calcular: 
    -> P(x<6) = 0.9389486


```r
dbinom(5, size=15, prob=0.2)+
dbinom(4, size=15, prob=0.2)+
dbinom(3, size=15, prob=0.2)+
dbinom(2, size=15, prob=0.2)+
dbinom(1, size=15, prob=0.2)+
dbinom(0, size=15, prob=0.2)
```

```
## [1] 0.9389486
```

b. Podemos encontrar la probabilidad de tener 8 y 3 preguntas correctas respondiendo de manera aleatoria usando:

Calcular: 
-> P(3<=x<=8) = 0.3510529


```r
summary(cars)
```

```
##      speed           dist       
##  Min.   : 4.0   Min.   :  2.00  
##  1st Qu.:12.0   1st Qu.: 26.00  
##  Median :15.0   Median : 36.00  
##  Mean   :15.4   Mean   : 42.98  
##  3rd Qu.:19.0   3rd Qu.: 56.00  
##  Max.   :25.0   Max.   :120.00
```

## Including Plots

You can also embed plots, for example:

![](DPDiscreta_files/figure-html/pressure-1.png)<!-- -->

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
