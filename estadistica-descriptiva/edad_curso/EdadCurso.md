---
title: "MOSEI31"
author: "Oscar Munoz"
date: "10/19/2018"
output: 
  html_document: 
    keep_md: yes
---



## Seguimiento curso Modelo de simulacion estadistico usando R.

Se evalua el siguiente conjunto de datos, basados en la edad de los estuidantes del curso.

La funcion summary retorna un conjuto de datos basicos:

```
##       Edad     
##  Min.   :15.0  
##  1st Qu.:25.0  
##  Median :28.0  
##  Mean   :27.6  
##  3rd Qu.:30.0  
##  Max.   :37.0
```

## Histograma de frecuencia

Se genera el historama de frecuencia con la fucion hist:

![](EdadCurso_files/figure-html/pressure-1.png)<!-- -->

## Funciones basicas para el analisis estadistico:

| Parametro         | Funcion                                         |
|-------------------|-------------------------------------------------|
| Numero de datos   | 60                         |
| Edad promedio     | 27.6                           |
| Mediana de Edad   | 28                         |
| Quartil 1 y 2     | 25, 28 |
| Percentil 50 y 80 | 28, 30  |
| Mininimo          | 15                            |
| Maximo            | 37                            |
| Desviacion Est    | 4.5517737                             |
| CV                | 0.1649193           |

## Concluciones:

- Se contabilizaron un total de 60 estudiantes, de donde el promedio de edad del curso de simulacion es de 27.6, con una desvicacion de 4.5 años.
- El valor maximo y minimo son 37 y 15 respectivamente.
- El 50% de los estudiantes tiene 28 años o menos.
- El 80% de los estudiantes tiene 30 o menos.

## Usefull links:

- [ESTADÍSTICA DESCRIPTIVA: REPRESENTACIONES GRÁFICAS](http://wpd.ugr.es/~bioestad/guia-r-studio/practica-2/)
