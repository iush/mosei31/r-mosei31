---
title: "MOSEI31"
author: "Oscar Munoz"
date: "10/19/2018"
output: 
  html_document: 
    keep_md: yes
---



## Seguimiento curso Modelo de simulacion estadistico usando R.

Se evalua el siguiente conjunto de datos, basados en la edad de los estuidantes del curso.

La funcion summary retorna un conjuto de datos basicos:

```
##        FECHA          TRM      
##  1/1/2015 :  1   Min.   :2362  
##  1/10/2015:  1   1st Qu.:2386  
##  1/11/2015:  1   Median :2456  
##  1/12/2015:  1   Mean   :2480  
##  1/13/2015:  1   3rd Qu.:2557  
##  1/14/2015:  1   Max.   :2678  
##  (Other)  :101
```

## Histograma de frecuencia

Se genera el historama de frecuencia con la fucion hist:

![](DATETRM_files/figure-html/pressure-1.png)<!-- -->

## Funciones basicas para el analisis estadistico:

| Parametro         | Funcion                                         |
|-------------------|-------------------------------------------------|
| Numero de datos   | 107                         |
| Edad promedio     | 2480.4846729                           |
| Mediana de Edad   | 2455.54                         |
| Quartil 1 y 2     | 2386.5, 2455.54 |
| Percentil 50 y 80 | 2455.54, 2574.02  |
| Mininimo          | 2361.54                            |
| Maximo            | 2677.97                            |
| Desviacion Est    | 91.1121458                             |
| CV                | 0.0367316           |

## Concluciones:

- Se contabilizaron un total de 60 estudiantes, de donde el promedio de edad del curso de simulacion es de 27.6, con una desvicacion de 4.5 años.
- El valor maximo y minimo son 37 y 15 respectivamente.
- El 50% de los estudiantes tiene 28 años o menos.
- El 80% de los estudiantes tiene 30 o menos.

## Usefull links:

- [ESTADÍSTICA DESCRIPTIVA: REPRESENTACIONES GRÁFICAS](http://wpd.ugr.es/~bioestad/guia-r-studio/practica-2/)
