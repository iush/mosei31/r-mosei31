---
title: "MOSEI31"
author: "Oscar Munoz"
date: "10/19/2018"
output: 
  html_document: 
    keep_md: yes
---


## Seguimiento curso Modelo de simulacion estadistico usando R.

Metropolitan Research, Inc., una organización para la investigación de consumo, realiza estudios que tienen por objeto evaluar una amplia variedad de productos y servicios para los consumidores.
En uno de sus estudios, Metropolitan se enfocó en la satisfacción de los consumidores con el funcionamiento de los automóviles producidos por el principal fabricante de Detroit. 
En cuestionario enviado a propietarios de automóviles de esta empresa se encontraron varias quejas relacionadas con problemas tempranos en la transmisión. 
Para tener más información acerca de los problemas en la transmisión, Metropolitan empleó una muestra de reparaciones de la transmisión proporcionada por empresas en Detroit, dedicadas a la reparación de transmisiones. 

Los datos siguientes son el número de milla recorridos por 48 automóviles hasta el momento en que se presentaron los problemas de transmisión.

Se evalua el siguiente conjunto de datos, basados en **las millas recorridas por los 48 automoviles hasta el problema de transmisión**.

Sea X = las millas recorridas por los 48 automoviles hasta el problema de transmisión

La funcion summary retorna un conjuto de datos basicos:

<table class="kable_wrapper">
<tbody>
  <tr>
   <td> 

| __ x __|
|-------:|
|   39323|
|   64242|
|   74276|
|   74425|
|   37831|
|   77539|

 </td>
   <td> 

| __ x __|
|-------:|
|   89641|
|   61978|
|   66998|
|   67202|
|   89341|
|   88798|

 </td>
   <td> 

| __ x __|
|-------:|
|   94219|
|   67998|
|   40001|
|  118444|
|   73341|
|   72000|

 </td>
   <td> 

| __ x __|
|-------:|
|  116803|
|   59817|
|   72069|
|   53500|
|   85288|
|   93500|

 </td>
   <td> 

| __ x __|
|-------:|
|   92857|
|  101769|
|   25066|
|   79294|
|  138114|
|  102000|

 </td>
   <td> 

| __ x __|
|-------:|
|   63436|
|   95774|
|   77098|
|   64544|
|   53402|
|   75683|

 </td>
   <td> 

| __ x __|
|-------:|
|   65605|
|  121352|
|   69922|
|   86813|
|   85586|
|   84690|

 </td>
   <td> 

| __ x __|
|-------:|
|   85861|
|   69568|
|   35662|
|  116269|
|   82256|
|  110452|

 </td>
  </tr>
</tbody>
</table>

```
##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
##   25066   65340   76390   78368   90445  138114
```

## Histograma de frecuencia

Se genera el historama de frecuencia con la fucion hist:

![](CarRepair_files/figure-html/pressure-1.png)<!-- -->

## Funciones basicas para el analisis estadistico:

| Parametro                  | Funcion                                                                           |
|----------------------------|-----------------------------------------------------------------------------------|
| Numero de datos            | 48                                                      |
| Media de millas recorridas | 78367.65                                     |
| Mediana millas             | 76390.5                                   |
| Quartil 1 y 2              | 65339.75, 76390.5, 90445     |
| Percentil 50 y 80          | 67281.6, 76390.5, 93931.4, 117869.65 |
| Mininimo                   | 25066                                                         |
| Maximo                     | 138114                                                         |
| Desviacion Est             | 23654.67                                       |
| CV                         | 0.3018423                                   |

## Concluciones:

- Se contabilizaron las millas recorridas de un total de 48 vehiculos, de donde el promedio de 78367.65 millas recorridas , con una desvicacion de 23654.67.
- El valor maximo y minimo son 138114 y 25066 respectivamente.
- El 50% de los vehiculos recorrieron 76390.5 millas antes de que la transmicion fallara.
- El 80% de los vehiculos fallaron antes de recorrer 93931.4 millas.

## Usefull links:

- [ESTADÍSTICA DESCRIPTIVA: REPRESENTACIONES GRÁFICAS](http://wpd.ugr.es/~bioestad/guia-r-studio/practica-2/)
