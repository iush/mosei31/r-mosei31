---
title: "MOSEI31"
author: "Oscar Munoz"
date: "10/19/2018"
output: 
  html_document: 
    keep_md: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
options(scipen = 999)
if(!require("readr")) install.packages("readr", repos="http://cran.us.r-project.org")
if(!require("tesseract")) install.packages("tesseract", repos="http://cran.us.r-project.org")
if(!require("magick")) install.packages("magick", repos="http://cran.us.r-project.org")
```
## Seguimiento curso Modelo de simulacion estadistico usando R.

Metropolitan Research, Inc., una organización para la investigación de consumo, realiza estudios que tienen por objeto evaluar una amplia variedad de productos y servicios para los consumidores.
En uno de sus estudios, Metropolitan se enfocó en la satisfacción de los consumidores con el funcionamiento de los automóviles producidos por el principal fabricante de Detroit. 
En cuestionario enviado a propietarios de automóviles de esta empresa se encontraron varias quejas relacionadas con problemas tempranos en la transmisión. 
Para tener más información acerca de los problemas en la transmisión, Metropolitan empleó una muestra de reparaciones de la transmisión proporcionada por empresas en Detroit, dedicadas a la reparación de transmisiones. 

Los datos siguientes son el número de milla recorridos por 48 automóviles hasta el momento en que se presentaron los problemas de transmisión.

Se evalua el siguiente conjunto de datos, basados en **las millas recorridas por los 48 automoviles hasta el problema de transmisión**.

Sea X = las millas recorridas por los 48 automoviles hasta el problema de transmisión

La funcion summary retorna un conjuto de datos basicos:
```{r cars, echo=FALSE}
edades <- read.csv("edades_curso.csv")
tranmicion <- read.csv("trabajo1.csv")

img <- image_read('table.png')
img_data <- ocr(img, engine = tesseract('eng', options = list(tessedit_char_whitelist = '0123456789',
                                                            tessedit_pageseg_mode = 'auto',
                                                            textord_tabfind_find_tables = '1',
                                                            textord_tablefind_recognize_tables = '1')))

#knitr::kable(img_data)
knitr::kable(list(col1=tranmicion$Oline[1:6], 
		  col2=tranmicion$Oline[7:12],
		  col3=tranmicion$Oline[13:18],
		  col4=tranmicion$Oline[19:24],
		  col5=tranmicion$Oline[25:30],
		  col6=tranmicion$Oline[31:36],
		  col7=tranmicion$Oline[37:42],
		  col8=tranmicion$Oline[43:48]
		  ), col.names="__ x __", row.names=FALSE)
summary(tranmicion$Oline)
#knitr::kable(tranmicion$Oline)
```

## Histograma de frecuencia

Se genera el historama de frecuencia con la fucion hist:

```{r pressure, echo=FALSE}
hist(tranmicion$Oline)
```

## Funciones basicas para el analisis estadistico:

| Parametro                  | Funcion                                                                           |
|----------------------------|-----------------------------------------------------------------------------------|
| Numero de datos            | `r length(tranmicion$Oline)`                                                      |
| Media de millas recorridas | `r round(mean(tranmicion$Oline), digits = 2)`                                     |
| Mediana millas             | `r round(median(tranmicion$Oline), digits = 2)`                                   |
| Quartil 1 y 2              | `r round(quantile(tranmicion$Oline, prob = c(0.25, 0.50, 0.75)), digits = 2)`     |
| Percentil 50 y 80          | `r round(quantile(tranmicion$Oline, prob = c(0.3, 0.5, 0.80, 0.95)), digits = 2)` |
| Mininimo                   | `r min(tranmicion$Oline)`                                                         |
| Maximo                     | `r max(tranmicion$Oline)`                                                         |
| Desviacion Est             | `r round(sd(tranmicion$Oline), digits = 2)`                                       |
| CV                         | `r sd(tranmicion$Oline)/mean(tranmicion$Oline)`                                   |

## Concluciones:

- Se contabilizaron las millas recorridas de un total de 48 vehiculos, de donde el promedio de `r round(mean(tranmicion$Oline), digits = 2)` millas recorridas , con una desvicacion de 23654.67.
- El valor maximo y minimo son `r max(tranmicion$Oline)` y `r min(tranmicion$Oline)` respectivamente.
- El 50% de los vehiculos recorrieron `r round(median(tranmicion$Oline), digits = 2)` millas antes de que la transmicion fallara.
- El 80% de los vehiculos fallaron antes de recorrer `r round(quantile(tranmicion$Oline, prob = c(0.80)), digits = 2 )` millas.

## Usefull links:

- [ESTADÍSTICA DESCRIPTIVA: REPRESENTACIONES GRÁFICAS](http://wpd.ugr.es/~bioestad/guia-r-studio/practica-2/)
